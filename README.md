### Description

JAVA REST API for game 15.
Technologies used:
- Spring Boot framework
- H2 in memory database
- JUnit and Mockito for testing

### Usage
Server listens at port 8080. All requests have to contain an instance of FifteenRequest with appropriate request type and valid user.
Services:
 - POST /api/newGame
    - Returns new game position
    - request type must be 'NEW_GAME'
    - Request example:
```json
  {
   "apiVersion": "1.0",
   "fifteenRequestType": "NEW_GAME",
   "fifteenRequestBody": {
   "user": {
      "id": 2,
      "userName": "Petras"
      }
    }
  }
```
    - Response example:
```json
{
  "fifteenResponseType": "SUCCESS",
  "fifteenResponseBody": {
    "recordList": [
      {
        "number": 2,
        "xcoordinate": 0,
        "ycoordinate": 0
      },
      {
        "number": 3,
        "xcoordinate": 0,
        "ycoordinate": 1
      },
      {
        "number": 7,
        "xcoordinate": 0,
        "ycoordinate": 2
      },
      {
        "number": 9,
        "xcoordinate": 0,
        "ycoordinate": 3
      },
      {
        "number": 15,
        "xcoordinate": 1,
        "ycoordinate": 0
      },
      {
        "number": 12,
        "xcoordinate": 1,
        "ycoordinate": 1
      },
      {
        "number": 0,
        "xcoordinate": 1,
        "ycoordinate": 2
      },
      {
        "number": 5,
        "xcoordinate": 1,
        "ycoordinate": 3
      },
      {
        "number": 4,
        "xcoordinate": 2,
        "ycoordinate": 0
      },
      {
        "number": 1,
        "xcoordinate": 2,
        "ycoordinate": 1
      },
      {
        "number": 13,
        "xcoordinate": 2,
        "ycoordinate": 2
      },
      {
        "number": 14,
        "xcoordinate": 2,
        "ycoordinate": 3
      },
      {
        "number": 6,
        "xcoordinate": 3,
        "ycoordinate": 0
      },
      {
        "number": 10,
        "xcoordinate": 3,
        "ycoordinate": 1
      },
      {
        "number": 11,
        "xcoordinate": 3,
        "ycoordinate": 2
      },
      {
        "number": 8,
        "xcoordinate": 3,
        "ycoordinate": 3
      }
    ],
    "boardList": [
      1
    ]
  },
  "message": ""
}
```	

 - POST /api/move
    - Performs a move by sending the position of moved tile and board number.
    - Request format is common for all requests. Different functions require different fields filled; these field are specified for every function.
    - request type must be 'MAKE_A_MOVE'.
    - Request example:
```json
{
	"apiVersion": "1.0",
	"fifteenRequestType": "MAKE_A_MOVE",
	"fifteenRequestBody": {
		"user": {
			"userId": 2,
			"userName": "Antanas"
		},
		"boardDTO": {
			"userId": 2,
			"boardId": 1
		},
		"move": {
			"number": 15,
			"ycoordinate": 2,
			"xcoordinate": 3
		}
	}
}
```

   - Response format (contains new position):
 ```json
 {
   "fifteenResponseType": "SUCCESS",
   "fifteenResponseBody": {
     "recordList": [
       {
         "xcoordinate": 0,
         "ycoordinate": 0,
         "number": 4
       },
       {
         "xcoordinate": 0,
         "ycoordinate": 1,
         "number": 0
       },
       {
         "xcoordinate": 0,
         "ycoordinate": 2,
         "number": 10
       },
       {
         "xcoordinate": 0,
         "ycoordinate": 3,
         "number": 8
       },
       {
         "xcoordinate": 1,
         "ycoordinate": 0,
         "number": 7
       },
       {
         "xcoordinate": 1,
         "ycoordinate": 1,
         "number": 9
       },
       {
         "xcoordinate": 1,
         "ycoordinate": 2,
         "number": 1
       },
       {
         "xcoordinate": 1,
         "ycoordinate": 3,
         "number": 5
       },
       {
         "xcoordinate": 2,
         "ycoordinate": 0,
         "number": 11
       },
       {
         "xcoordinate": 2,
         "ycoordinate": 1,
         "number": 3
       },
       {
         "xcoordinate": 2,
         "ycoordinate": 2,
         "number": 13
       },
       {
         "xcoordinate": 2,
         "ycoordinate": 3,
         "number": 6
       },
       {
         "xcoordinate": 3,
         "ycoordinate": 0,
         "number": 2
       },
       {
         "xcoordinate": 3,
         "ycoordinate": 1,
         "number": 15
       },
       {
         "xcoordinate": 3,
         "ycoordinate": 2,
         "number": 14
       },
       {
         "xcoordinate": 3,
         "ycoordinate": 3,
         "number": 12
       }
     ],
     "boardList": []
   },
   "message": ""
 }
```
 - POST /api/boards
    - Lists all games for particular user
    - Parameters:
        - user - id of the user who is registered, pre-defined values are 1,2,3.
        - request type must be 'GAMES_BY_USER'
```json
{
	"apiVersion": "1.0",
	"fifteenRequestType": "GAMES_BY_USER",
	"fifteenRequestBody": {
		"user": {
			"userId": 2
		}
	}
}
```        
    - response:
```json
{
  "fifteenResponseType": "SUCCESS",
  "fifteenResponseBody": {
    "recordList": [],
    "boardList": [
      1,
      18,
      35
    ]
  },
  "message": ""
}
```            
         
 - POST /api/position
    - Returns a position of all tiles for particular board
    - Parameters:
        - user
        - board id
        - 
```json
{
	"apiVersion": "1.0",
	"fifteenRequestType": "GAME_BY_ID",
	"fifteenRequestBody": {
		"user": {
			"userId": 2
		},
			"boardDTO": {
			"userId": 2,
			"boardId": 1
		}
	}
}
```
  - Response is the same as 'new game' response.                                                    

### Launch
To start please unzip the archive file and run the maven task:

```console
unzip fifteen.zip
cd fifteen
mvn clean install spring-boot:run
```

Swagger UI can be used for testing:

http://localhost:8080/swagger-ui.html

H2 database can be queried at:

http://localhost:8080/h2-console

### Assumptions
   - There is no user management and no authentication
   - User can have multiple games

### Enhancements
   - H2 in-memory database suits for temporary use only; however in can be easily upgraded to H2-in-file database or MySQL
   - Security is lacking completely; authentication and IP based restrictions are needed.
