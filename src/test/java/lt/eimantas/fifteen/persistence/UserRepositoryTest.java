package lt.eimantas.fifteen.persistence;

import lt.eimantas.fifteen.entity.persistence.Board;
import lt.eimantas.fifteen.entity.persistence.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

@RunWith(SpringRunner.class)
@DataJpaTest
@ActiveProfiles("test")
public class UserRepositoryTest {

    @Autowired
    TestEntityManager entityManager;

    @Autowired
    UserRepository userRepository;

    @Test
    public void testSaveFind() {
        User user1 = new User();
        user1.setUserName("username1");
        Board board = new Board();
        user1.getBoards().add(board);
        user1 = entityManager.persistAndFlush(user1);
        assertEquals(1, user1.getBoards().size());
        Board boardDb = user1.getBoards().get(0);
        assertNotNull(boardDb);
    }
}