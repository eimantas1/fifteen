package lt.eimantas.fifteen.controller;

import lt.eimantas.fifteen.entity.dto.FifteenResponse;
import lt.eimantas.fifteen.entity.helpers.FifteenResponseHelper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(FifteenController.class)
public class FifteenControllerTest {

    @Autowired
    WebApplicationContext wContext;
    private MockMvc mockMvc;
    @MockBean
    private FifteenController controller;

    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(wContext)
                .alwaysDo(MockMvcResultHandlers.print())
                .build();
    }

    @Test
    public void newGameTest() throws Exception {
        FifteenResponse response = FifteenResponseHelper.createSuccessResponse();
        Mockito.when(controller.newGame(any())).thenReturn(response);

        mockMvc.perform(post("/api/newGame")
                .characterEncoding("UTF-8")
                .content("{}")
                .contentType("application/json"))
                .andExpect(status().isOk());


    }
}