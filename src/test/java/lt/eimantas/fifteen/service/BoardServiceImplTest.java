package lt.eimantas.fifteen.service;

import lt.eimantas.fifteen.entity.dto.BoardDTO;
import lt.eimantas.fifteen.entity.dto.FifteenRequest;
import lt.eimantas.fifteen.entity.dto.FifteenRequestBody;
import lt.eimantas.fifteen.entity.dto.FifteenRequestType;
import lt.eimantas.fifteen.entity.dto.FifteenResponse;
import lt.eimantas.fifteen.entity.dto.PositionDTO;
import lt.eimantas.fifteen.entity.dto.UserDTO;
import lt.eimantas.fifteen.entity.helpers.BoardHelper;
import lt.eimantas.fifteen.entity.persistence.Board;
import lt.eimantas.fifteen.entity.persistence.Position;
import lt.eimantas.fifteen.entity.persistence.User;
import lt.eimantas.fifteen.mapper.PositionMapper;
import lt.eimantas.fifteen.persistence.BoardRepository;
import lt.eimantas.fifteen.persistence.PositionRepository;
import lt.eimantas.fifteen.persistence.UserRepository;
import lt.eimantas.fifteen.utils.FifteenRequestTestUtil;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static lt.eimantas.fifteen.entity.dto.FifteenResponseType.FAILURE;
import static lt.eimantas.fifteen.entity.dto.FifteenResponseType.SUCCESS;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class BoardServiceImplTest {

    @InjectMocks
    private final BoardServiceImpl boardService = new BoardServiceImpl();

    @Spy
    private PositionMapper positionMapper;

    @Mock
    private BoardRepository boardRepository;

    @Mock
    private PositionRepository positionRepository;

    @Mock
    private UserRepository userRepository;

    @Mock
    private PositionService positionService;

    private User user;

    @Before
    public void setUp() {
        user = new User();
        user.setUserId(1);
        user.setUserName("Username");
        Optional<User> optionalUser = Optional.of(user);
        when(userRepository.findById(any())).thenReturn(optionalUser);
    }

    @Test
    public void newGameTest() {
        Board board = new Board();
        board.setBoardId(1);
        when(boardRepository.save(any())).thenReturn(board);

        when(positionService.isSolvable(any())).thenReturn(true);

        FifteenRequest newGameRequest = FifteenRequestTestUtil.createNewGameRequest();
        FifteenResponse response = boardService.newGame(newGameRequest);
        assertNotNull(response);
        verify(boardRepository, times(1)).save(any());
    }

    @Test
    public void newGameTestBadRequest() {
        FifteenRequest newGameRequest = FifteenRequestTestUtil.createListBoardsRequest();
        FifteenResponse response = boardService.newGame(newGameRequest);
        assertNotNull(response);
        verify(boardRepository, times(0)).save(any());
    }

    @Test
    public void boardsByUserTest() {
        FifteenRequest request1 = FifteenRequestTestUtil.createListBoardsRequest();
        FifteenResponse response1 = boardService.getBoardsByUser(request1);
        assertNotNull(response1);
        assertEquals(SUCCESS, response1.getFifteenResponseType());

        FifteenRequest request2 = FifteenRequestTestUtil.createNewGameRequest();
        FifteenResponse response2 = boardService.getBoardsByUser(request2);
        assertNotNull(response2);
        assertEquals(FAILURE, response2.getFifteenResponseType());
        assertEquals("Incorrect request type for endpoint.", response2.getMessage());
    }

    @Test
    public void getPositionByBoardTest() {
        Board board = new Board();
        board.setBoardId(1);
        Optional<Board> boardOptional = Optional.of(board);
        when(boardRepository.findById(any())).thenReturn(boardOptional);

        FifteenRequest request = FifteenRequestTestUtil.createPositionByBoardRequest();
        FifteenResponse response = boardService.getPositionByBoard(request);
        assertNotNull(response);
        assertEquals(SUCCESS, response.getFifteenResponseType());
        verify(boardRepository, times(0)).save(any());
        verify(boardRepository, times(1)).findById(any());
    }

    @Test
    public void validateMoveTest() {
        PositionDTO move1 = new PositionDTO(3, 4, 15);
        List<Position> finalPositionDb = BoardHelper.FINAL_POSITION
                .stream()
                .map(p -> positionMapper.positionDtoToDataRecord(p, 1))
                .collect(Collectors.toList());

        Board board = new Board();
        board.getPositions().addAll(finalPositionDb);
        board.setBoardId(1);
        Optional<Board> boardDb = Optional.of(board);
        when(boardRepository.findById(any())).thenReturn(boardDb);
        BoardDTO boardDTO = new BoardDTO(1, 1);
        boolean result = boardService.validateMove(move1, boardDTO);

        assertFalse(result); //tile we are trying to press is out of bounds

        PositionDTO move2 = new PositionDTO(3, 0, 15);
        result = boardService.validateMove(move2, boardDTO);//tile is too far away from the empty space
        assertFalse(result);

        PositionDTO move3 = new PositionDTO(3, 3, 15);
        result = boardService.validateMove(move3, boardDTO);//tile is too far away from the empty space
        assertFalse(result);

        PositionDTO move4 = new PositionDTO(3, 0, 15);
        result = boardService.validateMove(move4, boardDTO);//trying to move the empty space itself
        assertFalse(result);

        PositionDTO move5 = new PositionDTO(2, 0, 15);
        result = boardService.validateMove(move5, boardDTO);//move tile 15
        assertTrue(result);

        PositionDTO move6 = new PositionDTO(3, 1, 12);
        result = boardService.validateMove(move6, boardDTO);//move tile 12
        assertTrue(result);
    }

    @Test
    public void makeMoveTest() {
        List<Position> finalPositionDb = BoardHelper.FINAL_POSITION
                .stream()
                .map(p -> positionMapper.positionDtoToDataRecord(p, 1))
                .collect(Collectors.toList());

        Board board = new Board();
        board.setBoardId(1);
        board.getPositions().addAll(finalPositionDb);
        Optional<Board> boardDb = Optional.of(board);
        when(boardRepository.findById(any())).thenReturn(boardDb);


        BoardDTO boardDTO = new BoardDTO(1, 1);
        PositionDTO move = new PositionDTO(0, 0, 1);
        UserDTO userDTO = new UserDTO(1, "username");
        FifteenRequestBody body = new FifteenRequestBody(userDTO, boardDTO, move);
        FifteenRequest request = new FifteenRequest("1.0", FifteenRequestType.MAKE_A_MOVE, body);
        FifteenResponse response = boardService.makeMove(request);
        assertNotNull(response);
        assertEquals(FAILURE, response.getFifteenResponseType());
        assertEquals("Move is not valid.", response.getMessage());

        PositionDTO move2 = new PositionDTO(2, 0, 15);
        FifteenRequestBody body2 = new FifteenRequestBody(userDTO, boardDTO, move2);
        FifteenRequest request2 = new FifteenRequest("1.0", FifteenRequestType.MAKE_A_MOVE, body2);
        response = boardService.makeMove(request2);
        assertNotNull(response);
        assertEquals(SUCCESS, response.getFifteenResponseType());
        assertEquals("", response.getMessage());
        assertFalse(response.getFifteenResponseBody().isSolved());


        FifteenRequest request3 = new FifteenRequest("1.0", FifteenRequestType.GAME_BY_ID, body2);
        response = boardService.makeMove(request3);
        assertNotNull(response);
        assertEquals(FAILURE, response.getFifteenResponseType());
        assertEquals("Incorrect request type for endpoint.", response.getMessage());

        PositionDTO move4 = new PositionDTO(3, 0, 15);
        FifteenRequestBody body4 = new FifteenRequestBody(userDTO, boardDTO, move4);
        FifteenRequest request4 = new FifteenRequest("1.0", FifteenRequestType.MAKE_A_MOVE, body4);
        response = boardService.makeMove(request4);
        assertNotNull(response);
        assertEquals(SUCCESS, response.getFifteenResponseType());
        assertEquals("", response.getMessage());

    }
}