package lt.eimantas.fifteen.service;

import lt.eimantas.fifteen.entity.dto.PositionDTO;
import lt.eimantas.fifteen.entity.helpers.BoardHelper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(MockitoJUnitRunner.class)
public class PositionServiceImplTest {

    @InjectMocks
    private final PositionService positionService = new PositionServiceImpl();

    @Test
    public void generateTest() {
        List<PositionDTO> board = positionService.generateNewBoard();
        assertTrue(positionService.isSolvable(board));
        assertEquals(16, board.size());
        List<Integer> numbers = board.stream().map(PositionDTO::getNumber).collect(Collectors.toList());
        assertEquals(16, numbers.size());
        Set<Integer> differentNumbers = new HashSet<>(numbers);
        assertEquals(16, differentNumbers.size());
    }

    @Test
    public void isSolvableTest() {
        assertTrue(positionService.isSolvable(BoardHelper.SIMPLE_SOLVABLE_POSITION));
        assertFalse(positionService.isSolvable(BoardHelper.SIMPLE_UNSOLVABLE_POSITION));
        assertTrue(positionService.isSolvable(BoardHelper.FINAL_POSITION));
    }

    @Test
    public void solvedTest() {
        List<PositionDTO> solvedPosition = Arrays.asList(
                new PositionDTO(0, 3, 1),
                new PositionDTO(1, 3, 2),
                new PositionDTO(2, 3, 3),
                new PositionDTO(3, 3, 4),
                new PositionDTO(0, 2, 5),
                new PositionDTO(1, 2, 6),
                new PositionDTO(2, 2, 7),
                new PositionDTO(3, 2, 8),
                new PositionDTO(0, 1, 9),
                new PositionDTO(1, 1, 10),
                new PositionDTO(2, 1, 11),
                new PositionDTO(3, 1, 12),
                new PositionDTO(0, 0, 13),
                new PositionDTO(1, 0, 14),
                new PositionDTO(3, 0, 0),
                new PositionDTO(2, 0, 15)
        );
        assertTrue(positionService.isSolved(solvedPosition));
        List<PositionDTO> unsolvedPosition = Arrays.asList(
                new PositionDTO(0, 3, 2),
                new PositionDTO(1, 3, 1),
                new PositionDTO(2, 3, 3),
                new PositionDTO(3, 3, 4),
                new PositionDTO(0, 2, 5),
                new PositionDTO(1, 2, 6),
                new PositionDTO(2, 2, 7),
                new PositionDTO(3, 2, 8),
                new PositionDTO(0, 1, 9),
                new PositionDTO(1, 1, 10),
                new PositionDTO(2, 1, 11),
                new PositionDTO(3, 1, 12),
                new PositionDTO(0, 0, 13),
                new PositionDTO(1, 0, 14),
                new PositionDTO(3, 0, 0),
                new PositionDTO(2, 0, 15)
        );
        assertFalse(positionService.isSolved(unsolvedPosition));
    }
}