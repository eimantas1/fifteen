package lt.eimantas.fifteen.utils;

import lt.eimantas.fifteen.entity.dto.UserDTO;

public class UserTestUtil {
    public static UserDTO createUser() {
        return new UserDTO(1, "Name");
    }
}
