package lt.eimantas.fifteen.utils;

import lt.eimantas.fifteen.entity.dto.BoardDTO;
import lt.eimantas.fifteen.entity.dto.FifteenRequest;
import lt.eimantas.fifteen.entity.dto.FifteenRequestBody;
import lt.eimantas.fifteen.entity.dto.FifteenRequestType;

public class FifteenRequestTestUtil {

    public static final String API_VERSION = "1.0";

    public static FifteenRequest createNewGameRequest() {
        FifteenRequestBody body = new FifteenRequestBody(UserTestUtil.createUser(), null, null);
        return new FifteenRequest(API_VERSION, FifteenRequestType.NEW_GAME, body);
    }

    public static FifteenRequest createListBoardsRequest() {
        FifteenRequestBody body = new FifteenRequestBody(UserTestUtil.createUser(), null, null);
        return new FifteenRequest(API_VERSION, FifteenRequestType.GAMES_BY_USER, body);
    }

    public static FifteenRequest createPositionByBoardRequest() {
        BoardDTO boardDTO = new BoardDTO(1, 1);
        FifteenRequestBody body = new FifteenRequestBody(UserTestUtil.createUser(), boardDTO, null);
        return new FifteenRequest(API_VERSION, FifteenRequestType.GAME_BY_ID, body);
    }
}
