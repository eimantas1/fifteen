package lt.eimantas.fifteen.entity.dto;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;

import static lt.eimantas.fifteen.entity.dto.FifteenRequestType.MAKE_A_MOVE;

public class FifteenRequestTest {

    @Test
    public void jsonTest() throws JsonProcessingException {
        ObjectMapper om = new ObjectMapper();
        UserDTO user = new UserDTO(1, "Antanas");
        BoardDTO board = new BoardDTO(1, 1);
        PositionDTO move = new PositionDTO(2, 0, 15);
        FifteenRequestBody body = new FifteenRequestBody(user, board, move);
        FifteenRequest request = new FifteenRequest("1.0", MAKE_A_MOVE, body);

        String requestString = om.writeValueAsString(request);

        String userString = om.writeValueAsString(user);
        om.readValue(userString, UserDTO.class);

        String positionString = om.writeValueAsString(move);
        om.readValue(positionString, PositionDTO.class);
        om.readValue(requestString, FifteenRequest.class);
    }
}