package lt.eimantas.fifteen.service;

import lt.eimantas.fifteen.entity.dto.BoardDTO;
import lt.eimantas.fifteen.entity.dto.FifteenRequest;
import lt.eimantas.fifteen.entity.dto.FifteenRequestType;
import lt.eimantas.fifteen.entity.dto.FifteenResponse;
import lt.eimantas.fifteen.entity.dto.PositionDTO;
import lt.eimantas.fifteen.entity.dto.UserDTO;
import lt.eimantas.fifteen.entity.helpers.FifteenResponseHelper;
import lt.eimantas.fifteen.entity.persistence.Board;
import lt.eimantas.fifteen.entity.persistence.Position;
import lt.eimantas.fifteen.entity.persistence.User;
import lt.eimantas.fifteen.exceptions.IncorrectRequestException;
import lt.eimantas.fifteen.mapper.PositionMapper;
import lt.eimantas.fifteen.persistence.BoardRepository;
import lt.eimantas.fifteen.persistence.PositionRepository;
import lt.eimantas.fifteen.persistence.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static lt.eimantas.fifteen.entity.helpers.FifteenResponseHelper.INCORRECT_REQUEST_MSG;
import static lt.eimantas.fifteen.entity.helpers.FifteenResponseHelper.MISSING_BOARD_NUMBER;
import static lt.eimantas.fifteen.entity.helpers.FifteenResponseHelper.UNKNOWN_USER_MSG;
import static lt.eimantas.fifteen.service.PositionServiceImpl.SQUARE_SIZE;

@Service
public class BoardServiceImpl implements BoardService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private BoardRepository boardRepository;

    @Autowired
    private PositionRepository positionRepository;

    @Autowired
    private PositionService positionService;

    @Autowired
    private PositionMapper positionMapper;

    @Autowired
    private EntityManager entityManager;

    @Override
    @Transactional
    public FifteenResponse newGame(FifteenRequest request) {
        try {
            validateRequest(request, FifteenRequestType.NEW_GAME);

            List<PositionDTO> positionDTOList = positionService.generateNewBoard();
            boolean isSolvable = positionService.isSolvable(positionDTOList);
            while (!isSolvable) {
                positionDTOList = positionService.generateNewBoard();
                isSolvable = positionService.isSolvable(positionDTOList);
            }

            Board board = new Board();
            board = boardRepository.save(board);

            Optional<User> user = userRepository.findById(request.getFifteenRequestBody().getUser().getUserId());
            user.get().getBoards().add(board);
            board.setUser(user.get());

            Integer boardId = board.getBoardId();

            board.getPositions().addAll(positionDTOList
                    .stream()
                    .map(m -> positionMapper.positionDtoToDataRecord(m, boardId))
                    .collect(Collectors.toList()));

            userRepository.save(user.get());

            FifteenResponse response = FifteenResponseHelper.createSuccessResponse();
            response.getFifteenResponseBody().getRecordList().addAll(positionDTOList);
            response.getFifteenResponseBody().getBoardList().add(boardId);
            return response;
        } catch (IncorrectRequestException ire) {
            return FifteenResponseHelper.createFailureResponse(ire.getMessage());
        }
    }

    private void validateRequest(FifteenRequest request, FifteenRequestType type) throws IncorrectRequestException {
        if (request.getFifteenRequestType() != type || request.getFifteenRequestBody() == null) {
            throw new IncorrectRequestException(INCORRECT_REQUEST_MSG);
        }
        UserDTO userDto = request.getFifteenRequestBody().getUser();
        if (userDto == null) {
            throw new IncorrectRequestException(INCORRECT_REQUEST_MSG);
        }
        Optional<User> user = userRepository.findById(userDto.getUserId());
        if (!user.isPresent()) {
            throw new IncorrectRequestException(UNKNOWN_USER_MSG);
        }
    }

    @Override
    @Transactional
    public FifteenResponse makeMove(FifteenRequest request) {
        try {
            validateRequest(request, FifteenRequestType.MAKE_A_MOVE);
            PositionDTO move = request.getFifteenRequestBody().getMove();
            BoardDTO boardDTO = request.getFifteenRequestBody().getBoardDTO();
            if (validateMove(move, boardDTO)) {
                saveMove(move, boardDTO);
                Board board = boardRepository.findById(boardDTO.getBoardId()).get();
                List<PositionDTO> positions = board.getPositions()
                        .stream()
                        .map(positionMapper::positionDataRecordToDto)
                        .collect(Collectors.toList());

                FifteenResponse response = FifteenResponseHelper.createSuccessResponse();
                response.getFifteenResponseBody().getRecordList().addAll(positions);
                response.getFifteenResponseBody().setSolved(positionService.isSolved(positions));
                return response;
            }
            return FifteenResponseHelper.createIncorrectMoveResponse();
        } catch (IncorrectRequestException ire) {
            return FifteenResponseHelper.createFailureResponse(ire.getMessage());
        }
    }

    private void saveMove(PositionDTO move, BoardDTO boardDto) {
        Integer currentValue = move.getNumber();
        Board board = boardRepository.findById(boardDto.getBoardId()).get();
        List<Position> positions = board.getPositions();
        Optional<Position> emptyPositionOptional = positions.stream().filter(p -> p.getNumber() == 0).findFirst();
        Position emptyPosition = emptyPositionOptional.get();
        emptyPosition.setNumber(currentValue);

        Optional<Position> currentPositionOptional = positions.stream().filter(p -> p.getNumber() == currentValue).findFirst();
        Position currentPosition = currentPositionOptional.get();
        currentPosition.setNumber(0);

        positionRepository.save(currentPosition);
        positionRepository.save(emptyPosition);
    }

    public boolean validateMove(PositionDTO move, BoardDTO boardDto) {
        Optional<Board> board = boardRepository.findById(boardDto.getBoardId());
        if (!board.isPresent()) {
            return false;
        }
        Integer xCoordinate = move.getXCoordinate();
        Integer yCoordinate = move.getYCoordinate();
        if ((xCoordinate > SQUARE_SIZE - 1) || (yCoordinate > SQUARE_SIZE - 1) ||
                (xCoordinate < 0) || (yCoordinate < 0)) {
            return false;
        }

        List<Position> positions = board.get().getPositions();
        Optional<Position> emptyPositionOptional = positions.stream().filter(p -> p.getNumber() == 0).findFirst();
        if (!emptyPositionOptional.isPresent()) {
            return false;
        }
        Position emptyPosition = emptyPositionOptional.get();
        Integer xDistance = emptyPosition.getXCoordinate() - xCoordinate;
        Integer yDistance = emptyPosition.getYCoordinate() - yCoordinate;
        return xDistance * xDistance + yDistance * yDistance == 1;
    }

    @Override
    public FifteenResponse getPositionByBoard(FifteenRequest request) {
        try {
            validateRequest(request, FifteenRequestType.GAME_BY_ID);
            BoardDTO boardDTO = request.getFifteenRequestBody().getBoardDTO();
            if (boardDTO == null || boardDTO.getBoardId() == null) {
                return FifteenResponseHelper.createFailureResponse(MISSING_BOARD_NUMBER);
            }

            Integer boardId = boardDTO.getBoardId();
            Board board = boardRepository.findById(boardId).get();
            List<Position> positions = board.getPositions();
            List<PositionDTO> positionsDto = positions
                    .stream()
                    .map(m -> positionMapper.positionDataRecordToDto(m))
                    .collect(Collectors.toList());

            FifteenResponse response = FifteenResponseHelper.createSuccessResponse();
            response.getFifteenResponseBody().getRecordList().addAll(positionsDto);
            return response;
        } catch (IncorrectRequestException ire) {
            return FifteenResponseHelper.createFailureResponse(ire.getMessage());
        }
    }

    @Override
    public FifteenResponse getBoardsByUser(FifteenRequest request) {
        try {
            validateRequest(request, FifteenRequestType.GAMES_BY_USER);

            Optional<User> userOptional = userRepository.findById(request.getFifteenRequestBody().getUser().getUserId());
            List<Board> boards = userOptional.get().getBoards();
            FifteenResponse response = FifteenResponseHelper.createSuccessResponse();
            response.getFifteenResponseBody().getBoardList().addAll(boards
                    .stream()
                    .map(Board::getBoardId)
                    .collect(Collectors.toList()));
            return response;

        } catch (IncorrectRequestException ire) {
            return FifteenResponseHelper.createFailureResponse(ire.getMessage());
        }
    }
}
