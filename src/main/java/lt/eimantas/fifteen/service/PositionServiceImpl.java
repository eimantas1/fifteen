package lt.eimantas.fifteen.service;

import lt.eimantas.fifteen.entity.dto.PositionDTO;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static lt.eimantas.fifteen.entity.helpers.BoardHelper.FINAL_POSITION;

@Service
public class PositionServiceImpl implements PositionService {

    public final static Integer SQUARE_SIZE = 4;

    @Override
    public List<PositionDTO> generateNewBoard() {
        List<Integer> randomNumbers = IntStream.range(0, SQUARE_SIZE * SQUARE_SIZE).boxed().collect(Collectors.toList());
        Collections.shuffle(randomNumbers);

        List<PositionDTO> positions = new ArrayList<>();
        for (int i = 0; i < SQUARE_SIZE; i++) {
            for (int j = 0; j < SQUARE_SIZE; j++) {
                Integer tileNumber = randomNumbers.get(i * SQUARE_SIZE + j);
                PositionDTO position = new PositionDTO(i, j, tileNumber);
                positions.add(position);
            }
        }
        return positions;
    }

    @Override
    public boolean isSolved(List<PositionDTO> positions) {
        return new HashSet<>(FINAL_POSITION).equals(new HashSet<>(positions));
    }

    public boolean isSolvable(List<PositionDTO> positions) {
        int sum = 0;
        for (PositionDTO position : positions) {
            int checkSum = position.getNumber() + position.getXCoordinate() + position.getYCoordinate();
            sum = sum + checkSum;
        }
        return sum % 2 == 0;
    }
}
