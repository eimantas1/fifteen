package lt.eimantas.fifteen.service;

import lt.eimantas.fifteen.entity.dto.FifteenRequest;
import lt.eimantas.fifteen.entity.dto.FifteenResponse;

public interface BoardService {
    FifteenResponse newGame(FifteenRequest fifteenRequest);
    FifteenResponse makeMove(FifteenRequest fifteenRequest);
    FifteenResponse getPositionByBoard(FifteenRequest fifteenRequest);
    FifteenResponse getBoardsByUser(FifteenRequest fifteenRequest);

}
