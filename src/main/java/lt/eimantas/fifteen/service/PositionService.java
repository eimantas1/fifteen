package lt.eimantas.fifteen.service;

import lt.eimantas.fifteen.entity.dto.PositionDTO;

import java.util.List;

public interface PositionService {
    List<PositionDTO> generateNewBoard();
    boolean isSolved(List<PositionDTO> positions);
    boolean isSolvable(List<PositionDTO> positions);
}
