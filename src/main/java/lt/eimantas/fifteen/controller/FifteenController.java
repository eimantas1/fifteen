package lt.eimantas.fifteen.controller;

import lt.eimantas.fifteen.entity.dto.FifteenRequest;
import lt.eimantas.fifteen.entity.dto.FifteenResponse;
import lt.eimantas.fifteen.service.BoardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class FifteenController {

    @Autowired
    private BoardService boardService;

    @PostMapping("/position")
    public FifteenResponse getPositionByBoard(@RequestBody @NonNull FifteenRequest fifteenRequest) {
        return boardService.getPositionByBoard(fifteenRequest);
    }

    @PostMapping("/boards")
    public FifteenResponse getBoardsByUser(@RequestBody @NonNull FifteenRequest fifteenRequest) {
        return boardService.getBoardsByUser(fifteenRequest);
    }

    @PostMapping("/move")
    public FifteenResponse makeMove(@RequestBody @NonNull FifteenRequest fifteenRequest) {
        return boardService.makeMove(fifteenRequest);
    }

    @PostMapping("/newGame")
    public FifteenResponse newGame(@RequestBody @NonNull FifteenRequest fifteenRequest) {
        return boardService.newGame(fifteenRequest);
    }
}
