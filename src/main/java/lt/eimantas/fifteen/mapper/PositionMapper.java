package lt.eimantas.fifteen.mapper;

import lt.eimantas.fifteen.entity.dto.PositionDTO;
import lt.eimantas.fifteen.entity.persistence.Position;
import org.springframework.stereotype.Component;

@Component
public class PositionMapper {


    public Position positionDtoToDataRecord(PositionDTO positionDTO, Integer boardId) {
        Position position = new Position();
        position.setXCoordinate(positionDTO.getXCoordinate());
        position.setYCoordinate(positionDTO.getYCoordinate());
        position.setNumber(positionDTO.getNumber());
        return position;
    }

    public PositionDTO positionDataRecordToDto(Position position) {
        return new PositionDTO(position.getXCoordinate(),
                position.getYCoordinate(),
                position.getNumber());
    }
}
