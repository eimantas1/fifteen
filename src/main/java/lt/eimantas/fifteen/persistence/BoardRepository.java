package lt.eimantas.fifteen.persistence;

import lt.eimantas.fifteen.entity.persistence.Board;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BoardRepository  extends JpaRepository<Board, Integer> {
}
