package lt.eimantas.fifteen.persistence;

import lt.eimantas.fifteen.entity.persistence.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {
}
