package lt.eimantas.fifteen.persistence;

import lt.eimantas.fifteen.entity.persistence.Position;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PositionRepository extends JpaRepository<Position, Integer> {
}
