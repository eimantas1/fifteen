package lt.eimantas.fifteen.entity.helpers;

import lt.eimantas.fifteen.entity.dto.FifteenResponse;

import static lt.eimantas.fifteen.entity.dto.FifteenResponseType.FAILURE;
import static lt.eimantas.fifteen.entity.dto.FifteenResponseType.SUCCESS;

public class FifteenResponseHelper {

    public static final String INCORRECT_REQUEST_MSG = "Incorrect request type for endpoint.";
    public static final String EMPTY_MSG = "";
    public static final String UNKNOWN_USER_MSG = "Unknown user in request.";
    private static final String INCORRECT_MOVE_MSG = "Move is not valid.";
    public static final String MISSING_BOARD_NUMBER = "Missing board number.";


    public static FifteenResponse createSuccessResponse() {
        return new FifteenResponse(SUCCESS, EMPTY_MSG);
    }

    public static FifteenResponse createIncorrectMoveResponse() {
        return new FifteenResponse(FAILURE, INCORRECT_MOVE_MSG);
    }

    public static FifteenResponse createFailureResponse(String msg) {
        return new FifteenResponse(FAILURE, msg);
    }
}
