package lt.eimantas.fifteen.entity.helpers;

import lt.eimantas.fifteen.entity.dto.PositionDTO;

import java.util.Arrays;
import java.util.List;

public class BoardHelper {

    public static final List<PositionDTO> FINAL_POSITION = Arrays.asList(
            new PositionDTO(0, 3, 1),
            new PositionDTO(1, 3, 2),
            new PositionDTO(2, 3, 3),
            new PositionDTO(3, 3, 4),
            new PositionDTO(0, 2, 5),
            new PositionDTO(1, 2, 6),
            new PositionDTO(2, 2, 7),
            new PositionDTO(3, 2, 8),
            new PositionDTO(0, 1, 9),
            new PositionDTO(1, 1, 10),
            new PositionDTO(2,1, 11),
            new PositionDTO(3,1, 12),
            new PositionDTO(0,0, 13),
            new PositionDTO(1,0, 14),
            new PositionDTO(2,0, 15),
            new PositionDTO(3,0, 0)
    );

    public static final List<PositionDTO> SIMPLE_SOLVABLE_POSITION = Arrays.asList(
            new PositionDTO(0, 3, 1),
            new PositionDTO(1, 3, 2),
            new PositionDTO(2, 3, 3),
            new PositionDTO(3, 3, 4),
            new PositionDTO(0, 2, 5),
            new PositionDTO(1, 2, 6),
            new PositionDTO(2, 2, 7),
            new PositionDTO(3, 2, 8),
            new PositionDTO(0, 1, 9),
            new PositionDTO(1, 1, 10),
            new PositionDTO(2,1, 11),
            new PositionDTO(3,1, 12),
            new PositionDTO(0,0, 13),
            new PositionDTO(1,0, 14),
            new PositionDTO(2,0, 0),
            new PositionDTO(3,0, 15)
    );

    public static final List<PositionDTO> SIMPLE_UNSOLVABLE_POSITION = Arrays.asList(
            new PositionDTO(0, 3, 1),
            new PositionDTO(1, 3, 2),
            new PositionDTO(2, 3, 3),
            new PositionDTO(3, 3, 4),
            new PositionDTO(0, 2, 5),
            new PositionDTO(1, 2, 6),
            new PositionDTO(2, 2, 7),
            new PositionDTO(3, 2, 8),
            new PositionDTO(0, 1, 9),
            new PositionDTO(1, 1, 10),
            new PositionDTO(2,1, 11),
            new PositionDTO(3,1, 12),
            new PositionDTO(0,0, 13),
            new PositionDTO(1,0, 15),
            new PositionDTO(2,0, 15),
            new PositionDTO(3,0, 0)
    );
}
