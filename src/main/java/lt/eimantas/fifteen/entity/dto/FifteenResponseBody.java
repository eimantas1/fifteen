package lt.eimantas.fifteen.entity.dto;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class FifteenResponseBody {
    private List<PositionDTO> recordList = new ArrayList<>();
    private List<Integer> boardList = new ArrayList<>();
    private boolean solved;
}
