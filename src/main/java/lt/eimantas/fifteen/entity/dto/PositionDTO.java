package lt.eimantas.fifteen.entity.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NonNull;
import lombok.ToString;

@Getter
@ToString
@EqualsAndHashCode
public class PositionDTO {
    @NonNull
    private final Integer xCoordinate;
    @NonNull
    private final Integer yCoordinate;
    @NonNull
    private final Integer number;

    @JsonCreator(mode = JsonCreator.Mode.PROPERTIES)
    public PositionDTO(@JsonProperty("xcoordinate") Integer xCoordinate,
                       @JsonProperty("ycoordinate") Integer yCoordinate,
                       @JsonProperty("number") Integer number) {
        this.xCoordinate = xCoordinate;
        this.yCoordinate = yCoordinate;
        this.number = number;
    }
}
