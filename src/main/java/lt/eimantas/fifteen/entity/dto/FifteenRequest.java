package lt.eimantas.fifteen.entity.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;


@Getter
public class FifteenRequest {
    private final String apiVersion;
    private final FifteenRequestType fifteenRequestType;
    private final FifteenRequestBody fifteenRequestBody;

    @JsonCreator(mode = JsonCreator.Mode.PROPERTIES)
    public FifteenRequest(@JsonProperty("apiVersion") String apiVersion,
                          @JsonProperty("fifteenRequestType") FifteenRequestType fifteenRequestType,
                          @JsonProperty("fifteenRequestBody") FifteenRequestBody fifteenRequestBody) {
        this.apiVersion = apiVersion;
        this.fifteenRequestBody = fifteenRequestBody;
        this.fifteenRequestType = fifteenRequestType;
    }
}

