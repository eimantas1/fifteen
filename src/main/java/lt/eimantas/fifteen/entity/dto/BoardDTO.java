package lt.eimantas.fifteen.entity.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;


@Getter
public class BoardDTO {
    private final Integer userId;
    private final Integer boardId;

    @JsonCreator(mode = JsonCreator.Mode.PROPERTIES)
    public BoardDTO(@JsonProperty("userId") Integer userId, @JsonProperty("boardId") Integer boardId) {
        this.userId = userId;
        this.boardId = boardId;
    }
}
