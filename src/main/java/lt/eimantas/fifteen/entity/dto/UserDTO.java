package lt.eimantas.fifteen.entity.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.ToString;


@Getter
@ToString
public class UserDTO {
    private final Integer userId;
    private final String userName;

    @JsonCreator(mode = JsonCreator.Mode.PROPERTIES)
    public UserDTO(@JsonProperty("userId") Integer userId, @JsonProperty("userName") String userName) {
        this.userId = userId;
        this.userName = userName;
    }
}
