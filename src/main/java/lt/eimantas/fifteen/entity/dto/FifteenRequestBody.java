package lt.eimantas.fifteen.entity.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;


@Getter
public class FifteenRequestBody {
    private final UserDTO user;
    private final BoardDTO boardDTO;
    private final PositionDTO move;

    @JsonCreator(mode = JsonCreator.Mode.PROPERTIES)
    public FifteenRequestBody(@JsonProperty("user") UserDTO user,
                              @JsonProperty("boardDTO") BoardDTO boardDTO,
                              @JsonProperty("move") PositionDTO move) {
        this.user = user;
        this.boardDTO = boardDTO;
        this.move = move;
    }
}