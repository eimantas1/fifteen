package lt.eimantas.fifteen.entity.dto;

public enum FifteenRequestType {
    GAMES_BY_USER,
    GAME_BY_ID,
    NEW_GAME,
    MAKE_A_MOVE
}

