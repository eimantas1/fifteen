package lt.eimantas.fifteen.entity.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class FifteenResponse {

    private final FifteenResponseType fifteenResponseType;
    private final FifteenResponseBody fifteenResponseBody;
    private final String message;

    public FifteenResponse(FifteenResponseType type, String msg) {
        fifteenResponseType = type;
        message = msg;
        fifteenResponseBody = new FifteenResponseBody();
    }
}
