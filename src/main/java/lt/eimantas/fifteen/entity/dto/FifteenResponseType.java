package lt.eimantas.fifteen.entity.dto;

public enum FifteenResponseType {
    INCORRECT_REQUEST, SUCCESS, FAILURE
}

