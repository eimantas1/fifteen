package lt.eimantas.fifteen.entity.persistence;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Data
@Entity
@NoArgsConstructor
public class Position {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer positionId;
    @NonNull
    private Integer xCoordinate;
    @NonNull
    private Integer yCoordinate;
    private Integer number;
 }
