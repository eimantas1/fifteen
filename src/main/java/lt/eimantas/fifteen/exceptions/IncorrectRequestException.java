package lt.eimantas.fifteen.exceptions;

public class IncorrectRequestException extends Exception {
    public IncorrectRequestException(String incorrectRequestMsg) {
        super(incorrectRequestMsg);
    }
}
